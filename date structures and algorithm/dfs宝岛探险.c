# include <stdio.h>//本程序坐标为0-n
int book[4][4]={0};
int a[4][4],sum;
void dfs(int x,int y){
	int next[4][2]={{1,0},{0,1},{0,-1},{-1,0}};
	int k,tx,ty;
	for(k=0;k<4;k++){
		tx=x+next[k][0];
		ty=y+next[k][1];
		if(tx<0||ty<0||tx>3||ty>3)
			continue;
		if(book[tx][ty]==0&&a[tx][ty]>0){
			book[tx][ty]=1;
			sum++;
			dfs(tx,ty);
		}

	}
	return;
}
int main(){
	int i,j,startx,starty;
	for(i=0;i<4;i++)
		for(j=0;j<4;j++)
			scanf("%d",&a[i][j]);
		printf("请输入初始位置");
		scanf("%d %d",&startx,&starty);
		book[startx][starty]=1;
		sum=1;
		dfs(startx,starty);
		printf("%d",sum);
		system("pause");
		return 0;
}