# include <stdio.h>
struct que{
	int x;
	int y;
	int date;
};
struct que q[100];
int h[100];
void quicksort(int left,int right){
	int l,i,j,k,t;
	struct que temp;
	if(left>=right)
		return ;
	i=left;
	j=right;
	while(i!=j){
		while(i<j&&q[j].date>=q[left].date)
			j--;
		while(i<j&&i>left&&q[i].date<=q[left].date)
			i++;
		if(q[i].date>q[j].date){
			temp=q[i];
			q[i]=q[j];
			q[j]=temp;
		}
	}
	temp=q[left];
	q[left]=q[i];
	q[i]=temp;
	quicksort(left,i-1);
	quicksort(i+1,right);
	return ;			//切记此处return是终止排序，上一个return是终止一次递归
}
int find(int r){
	while(r!=h[r]){
		h[r]=find(h[r]);
		return h[r];
	}
	return r;
}
int test(int a,int b){
	int ta;
	int tb;
	ta=find(a);
	tb=find(b);
	if(ta!=tb){
		h[tb]=ta;
		return 0;	//先修改祖先，在返回，不然返回了，就不修改了
	}
	else
		return 1;
}
int main(){
	int m,n,i,j,k,count,sum=0;
	printf("请输入结点个数，边的个数：\n");
	scanf("%d %d",&m,&n);
	for(j=0;j<=m;j++)
		h[j]=j;
	for(i=1;i<=n;i++){
		scanf("%d %d %d",&q[i].x,&q[i].y,&q[i].date);
	}
	quicksort(1,n);
	for(i=1;i<=n;i++)
		printf("%d  ",q[i].date);
	for(k=1;k<=n;k++)	
		if(test(q[k].x,q[k].y)==0)
			sum+=q[k].date;
	printf("\n %d",sum);
	system("pause");
	return 0;
}