# include<stdio.h>		//注意，book是标记是否走过的路线，是相对于总数的，故是一位数组。而a[]是表示节点之间的关系的，是二维数组
int a[10][10],book[10]={0},m,sum=0;
void dfs(int in){
	int i;
	printf("%d",in);
	sum++;
	if(sum==m)
		return ;
	for(i=1;i<=m;i++){
		if(a[in][i]==1&&book[i]==0){
			book[i]=1;
			dfs(i);
		}
	}
	return ;
}
int main(){
	int i,j,x,y,n;
	printf("请输入节点数量,");
	scanf("%d %d",&m,&n);
	for(i=1;i<m;i++)
		for(j=1;j<=m;j++)
			if(i==j)
				a[i][j]=0;
			else
				a[i][j]=9999;
	for(i=1;i<=n;i++){
		scanf("%d %d",&x,&y);
		a[x][y]=a[y][x]=1;
	}
	book[1]=1;
	dfs(1);
	system("pause");
	return 0;
}