# include <stdio.h>
int h[100]={0},n,m;
int find(int r){
	while(r!=h[r]){
		h[r]=find(h[r]);  //递归为祖先的值
		return h[r];	//返回该元素父亲的值，从而寻找祖先的值
	}
	return r;	//导出祖先的值
}
void test(int a,int b){
	int t1,t2;
	t1=find(a);
	t2=find(b);
	if(h[t1]!=h[t2])
		h[t2]=t1;
}
int main(){
	int j,k,a,b,sum=0;
	printf("请输入元素数量、关系数量:\n");
	scanf("%d %d",&m,&n);
	for(j=1;j<=m;j++)
		h[j]=j;
	for(k=1;k<=n;k++){
		scanf("%d %d",&a,&b);
		test(a,b);
	}
	for(j=1;j<=m;j++)
		printf("h[%d]：%d  ",j,h[j]);
	for(k=1;k<=m;k++)
		if(h[k]==k)
			sum++;
		printf("\n%d",sum);
	system("pause");
	return 0;
}