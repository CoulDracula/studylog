# include <stdio.h>//计算n个数字有多少种排序方法
int a[10],book[10],n;
void dfs(int step){
	int i;
	if(step==n+1){
		for(i=1;i<=n;i++)
			printf("%d ",a[i]);
		printf("\n");
		return ;
	}
	for(i=1;i<=n;i++){
		if(book[i]==0){
			a[step]=i;
			book[i]=1;
			dfs(step+1);
			book[i]=0;	//为什么要返回book[i]=0 ?一次排序后，要返回book都为0；进行下一次筛选
		}
	}
	return ;
}
int main(){
	scanf("%d",&n);
	dfs(1);
	system("pause");
	return 0;
}